﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDotNet
{
    class Ville
    {
        public string Nom { get; set; }
        public int Population { get; set; }
        public double Superficie { get; set; }
        public Departement Departement { get; set; }

        public double Densite => Population / Superficie;

        public Ville(string nom, int population, double superficie, Departement departement)
        {
            Nom = nom;
            Population = population;
            Superficie = superficie;
            Departement = departement;
        }

        public Ville() 
        {
        }


    }

}
