﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDotNet
{
    class Avion
    {
        #region ATTRIBUTES
        private int envergure;
        private int capacite;

        #endregion

        #region PROPERTIES
        public int Envergure
        {
            get
            {
                return this.envergure;
            }
            set
            {
                if (value > 0)
                {
                    this.envergure = value;
                }
                else
                {
                    Console.WriteLine("Envergure incorrecte");
                    throw new Exception("Envergure incorrecte");
                }
            }
        }

        public int Capacite { get => capacite; set => capacite = value; }
        //public int Capacite { get { return capacite; } set { capacite = value; } }

        public string Modele { get; set; }
        #endregion




        #region CONSTRUCTEURS
        public Avion(int envergure = 55, int capacite = 200, string modele = "Boeing 747")
        {
            this.envergure = envergure;
            this.Capacite = capacite;
            this.Modele = modele;
        }

        public Avion(int envergure, String nom) : this(envergure, 1500, nom)
        {

        }

        // surcharge de constructeur
        public Avion() : this(50, 200, "Airbus A310")
        {

        }
        #endregion

        #region METHODS
        public void FaireQuelqueChose()
        {
            #region Etape 1
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            #endregion

            #region Etape 2
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            #endregion

            #region étape finale
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            // dhqskjdhqs k
            #endregion
        }
        #endregion
    }
}
